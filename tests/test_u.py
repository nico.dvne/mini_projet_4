import unittest
from def_funct import CreerGrille,getColor,MajForet

class TestsUnitaires(unittest.TestCase):
    
    def TestsCreate(self):
        self.assertEqual(CreerGrille(2,2),[[0,0],[0,0]])

    def TestColor(self):
        self.assertEqual(getColor(2),'red')
    
    def TestMajForet(self):
        grille = CreerGrille(2,2)
        for ligne in range(len(grille)):
            for colonne in range(len(grille[0])):
                grille[ligne][colonne] = 2
        
        MajForet(grille)

        self.assertEqual(grille[1][1],3)

if __name__ == "__main__":
    unittest.main()