import argparse

def arguments_script():
    parser = argparse.ArgumentParser()
    parser.add_argument("-rows",type = int)
    parser.add_argument("-colums",type = int)
    parser.add_argument("-duration",type = float)
    parser.add_argument("-cell_size",type = int)
    parser.add_argument("-afforestation",type = float)
    argument = parser.parse_args()
    return argument

def f_rows(argument):
    rows = 20
    if(argument.rows is not None):
        if(argument.rows < 1 or argument.rows > 30):
            erreur_rows="Nombre de lignes entre 1 et 30, Type : Entier"
            raise argparse.ArgumentTypeError(erreur_rows)
        else:
            rows = argument.rows
    return rows

def f_columns(argument):
    colums = 20
    if(argument.colums is not None):
        if(argument.colums < 1 or argument.colums > 30):
            erreur_rows="Nombre de lignes entre 1 et 30, Type : Entier"
            raise argparse.ArgumentTypeError(erreur_rows)
        else:
            colums = argument.colums
    return colums    

def f_cell_size(argument):
    cell_size = 25
    if(argument.cell_size is not None):
        if(argument.cell_size < 10 or argument.cell_size > 50):
            erreur_size="taille des cellules entre 10 et 50 (35 par défaut), Type : Entier"
            raise argparse.ArgumentTypeError(erreur_size)
        else:
            cell_size = argument.cell_size
    return cell_size

def f_afforestation(argument):
    afforestation = 0.6
    if(argument.afforestation is not None):
        argument.afforestation = argument.afforestation
        if(argument.afforestation < 0 or argument.afforestation > 1):
            erreur_proba="Probabilité que la cellule soit un arbre entre 0 et 1 (0.6 par défaut), Type : Réel"
            raise argparse.ArgumentTypeError(erreur_proba)
        else :
            afforestation = argument.afforestation
    return afforestation

def f_duration(argument):
    duration = 3
    if(argument.duration is not None):
        argument.duration = float(argument.duration)
        if(argument.duration < 0.1 or argument.duration > 5.0):
            erreur_time="Nombre de seconde de l'animation entre 0 et 5 (2 par défaut), Type : Réel"
            raise argparse.ArgumentTypeError(erreur_time)
        else :
            duration = argument.duration
    return duration
