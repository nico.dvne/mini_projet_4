#!/usr/bin/env python 

from tkinter import *
from def_funct import *
import arguments
import sys

args = arguments.arguments_script()
rows = arguments.f_rows(args)
columns = arguments.f_columns(args)
cell_size = arguments.f_cell_size(args)
afforestation = arguments.f_afforestation(args)
duration = arguments.f_duration(args)

window = Tk()
window.config(bg='grey')
window.title("Fire Simulation 2K20")
canvas = Canvas(window, width=rows*cell_size, height=rows*cell_size)
canvas.grid(row=1, column=0, columnspan=3, pady=30, padx=30)

'''
IntVar(master=X) est un outil de tkinter permettant d'attribuer une valeur sur un Checkbutton.
0 si bouton pas cliqué et 1 si cliqué
On se servira de cette variable plus tard pour activer le script ( a travers bruler.get ) quand le bouton sera cliqué
'''
bruler = IntVar(master=window)

def quitter():
    sys.exit()

def renew():
    CreerGrille(rows,columns)
    initialisation(grille_de_jeu,0.6)
    MaJCanvas(grille_de_jeu)



btn_new = Button(window,text="Nouvelle carte",command=renew)
btn_new.grid(row = 0,column = 1)

btn_bruler = Checkbutton(window,text="FIRE !!!",indicatoron=0,variable=bruler)
btn_bruler.grid(row = 0 , column = 0,padx = 5,pady = 5)

btn_quit = Button(window,text="Quitter",command=quitter)
btn_quit.grid(row = 0 , column = 2,padx = 5,pady = 5)

'''
Cette fonction dessine une case sur le canvas en fonction de la valeur de la case
Nos parametres pos_X et pos_Y nous permettent de localisé le carré sur la carte
pos_X * cell_size/ pos_Y * cell_size et (pos_X + 1) * cell_size/(pos_Y + 1) * cell_size nous permettent d'appliquer le carré sur le canvas 
on divise pos_X et pos_Y par cell_size afin d'adapter la position par rapport au canvas

'''
def DessinerRectangle(pos_X,pos_Y,value):
    couleur = getColor(value)
    canvas.create_rectangle(pos_X * cell_size, pos_Y * cell_size, (pos_X + 1) * cell_size,
                            (pos_Y + 1) * cell_size, fill=couleur)


'''
MajCanvas appelle pour chaque case de la grille la fonction DessinerRectangle 
'''
def MaJCanvas(grille):
    for ligne in range(len(grille)):
        for colonne in range(len(grille[0])):
            if grille[ligne][colonne]:
                DessinerRectangle(colonne, ligne, grille[ligne][colonne])





'''
Cette fonction sera appelée a chaque clique gauche sur le canvas
Si la valeur de la case est 1 ( un arbre ), alors on l'enflamme en passant la valeur a 2
Alors, on met a jour la carte
On caste x1 et y1 en int afin de recuperer uniquement les parties entiere de la division ( pas d'index float dans les listes )
'''
def click_callback(event):
    x1 = int(event.x / cell_size)
    y1 = int(event.y / cell_size)
    if grille_de_jeu[y1][x1] == 1:
        grille_de_jeu[y1][x1] = 2
        MaJCanvas(grille_de_jeu)

canvas.bind('<Button-1>', click_callback)


'''
Methode recursive qui met a jour la carte et l'affiche dans le canvas si le bouton bruler est enfoncé
La methode attends 10ms puis recommence l'opération ( recursivité )
'''
def move():
    if bruler.get() == 1 :
        MajForet(grille_de_jeu)
        MaJCanvas(grille_de_jeu)
    #Le windows.After permet de ne pas avoir l'erreur de recursion maximum
    # ( merci les potes pour le tuyau )
    window.after(10,move)


grille_de_jeu = CreerGrille(rows,columns)
initialisation(grille_de_jeu,0.7)
MaJCanvas(grille_de_jeu)
move()

window.mainloop()

