# Fire simulation

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) 

Ce projet consiste à créer une simulation de feu de forêt via python.

## Fabriqué avec

* [Python](https://www.python.org/)
* [Tkinter](http://tkinter.fdex.eu/)

## Auteurs

* **Nicolas Davenne** _alias_ [@nico.dvne](https://gitlab.com/nico.dvne)

* **Julien Peltot** _alias_ [@Julien_Peltot](https://gitlab.com/Julien_Peltot)

