
from random import *

'''
Cette fonction créer une liste de liste, ressemblant a une matrice
Notre grille de jeu pour CreateGrille(2,2) ressemblera donc a [0,0],[0,0]
Nous utilisons 2 specificitée du python : 
                                        [[]]*N creer une liste contenant N liste
                                        [0]*N rempli la ligne de la liste de N fois 0 soit [0]*3 = [0,0,0]
On obtient donc des matrices grace a cette methode

'''
def CreerGrille(ligne,colonne):
    grille = [[]] * ligne
    for i_ligne in range(0,ligne):
        grille[i_ligne] = [0] * colonne
    return grille


'''
La fonction initialisation prends en parametre la grille de jeu et la proba que la case soit une foret.
Pour chaque case de la 'matrice', on effectue un tirage aleatoire entre 0 et 1.
Si le tirage est inférieur ou égal a la proba, alors on met un 1 dans la case ( correspond a un arbre )
'''
def initialisation(grille,afforestation):
    for ligne in range(len(grille)):
        for colonne in range(len(grille[0])):
            tirage = random()
            if tirage <= afforestation:
                grille[ligne][colonne] = 1



'''

    La fonction de voisinnage est une piece maitresse de l'algorithme de Van Neumann
    Celle ci retourne une liste de 4 elements : 1:Case inférieure / 2: Case supérieure
                                                3:Case Droite / 4: Case Gauche
    
    La liste est composée soit de 0, soit de 1. O si pas de voisinnage possible / 1 sinon

'''
def voisinnage(grille, ligne, col):
    l_voisins = [0] * 4
    if ligne+1 < len(grille):
        l_voisins[grille[ligne+1][col]] += 1
    if ligne-1 >= 0:
        l_voisins[grille[ligne-1][col]] += 1
    if col+1 < len(grille[0]):
        l_voisins[grille[ligne][col+1]] += 1
    if col-1 >=0:
        l_voisins[grille[ligne][col-1]] += 1

    return l_voisins




'''
Cette fonction 'met a jour' la grile de jeu. Pour cela nous créeons une grille similaire ( un buffer ). 
Et , pour chaque case de la liste, on check l'etat de la case
Pour rappel, dans ce programme : 
                                0 => Plaine
                                1 => Arbre
                                2 => Feu
                                3 => Cendre
On verifie donc , pour chaque case l'etat est 1 ou 2
On traite que les 1 et 2 car une plaine ne peux pas bruler, et des cendres restent des cendres...
Cependant, si une case passe de l'etat arbre => feu , il est necessaire qu'un des voisins soit en feu ( d'ou l'appel a voisinnage )


Pour finir, on enregistre le buffer dans la grille de jeu afin de la mettre a jour
'''
def MajForet(grille_de_jeu):
    buffer = CreerGrille(len(grille_de_jeu),len(grille_de_jeu[0]))
    for ligne in range(len(grille_de_jeu)):
        for colonne in range(len(grille_de_jeu[0])):
            buffer[ligne][colonne] = grille_de_jeu[ligne][colonne]
            if grille_de_jeu[ligne][colonne] == 2:
                buffer[ligne][colonne] = 3
            if grille_de_jeu[ligne][colonne] == 1 and voisinnage(grille_de_jeu,ligne,colonne)[2] >0:
                buffer[ligne][colonne] = 2
    
    for ligne in range(len(grille_de_jeu)):
        for colonne in range(len(grille_de_jeu[0])):
            grille_de_jeu[ligne][colonne] = buffer[ligne][colonne]

'''
Cette fonction retourne une chaine de caractère en fonction de la valeur de la case de la liste
'''
def getColor(value):
    color = ""
    if value == 1:
        color = "#004d00"
    elif value == 2:
        color = "red"
    elif value == 3:
        color = "#404040"
    else:
        color = "black"
    return color